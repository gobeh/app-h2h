package com.example.apph2h.exception;

public class InvoiceNumberAlreadyExistsException extends Exception {
    public InvoiceNumberAlreadyExistsException(String message) {
        super(message);
    }
}
