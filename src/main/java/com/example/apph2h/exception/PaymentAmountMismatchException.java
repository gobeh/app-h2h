package com.example.apph2h.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PaymentAmountMismatchException extends PaymentServiceException {
    public PaymentAmountMismatchException(String message){
        super(message);
    }
}
