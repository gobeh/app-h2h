package com.example.apph2h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppH2hApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppH2hApplication.class, args);
    }

}
