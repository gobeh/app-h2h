package com.example.apph2h.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PaymentServiceException extends Exception {
    public PaymentServiceException(String msg){
        super(msg);
    }
}
