package com.example.apph2h.exception;

public class VirtualAccountNumberAlreadyExistsException extends Exception {
    public VirtualAccountNumberAlreadyExistsException(String message) {
        super(message);
    }
}
