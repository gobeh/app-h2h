package com.example.apph2h.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class VirtualAccountAlreadyPaidException extends PaymentServiceException {
    public VirtualAccountAlreadyPaidException(String msg){
        super(msg);
    }
}
