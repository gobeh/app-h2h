package com.example.apph2h.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidRequestException extends PaymentServiceException {
    public InvalidRequestException(String msg){
        super(msg);
    }
}
