package com.example.apph2h.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class VirtualAccountNotFoundException extends PaymentServiceException {
    public VirtualAccountNotFoundException(String message) {
        super(message);
    }
}
